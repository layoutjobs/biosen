<?php $active = rand(1, 3); ?>

<div class="banner">
    <div class="container">
        <div id="banner-carousel" class="carousel" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#banner-carousel" data-slide-to="0"<?php echo $active === 1 ? ' class="active"' : ''; ?>></li>
                <li data-target="#banner-carousel" data-slide-to="1"<?php echo $active === 2 ? ' class="active"' : ''; ?>></li>
                <li data-target="#banner-carousel" data-slide-to="2"<?php echo $active === 3 ? ' class="active"' : ''; ?>></li>
            </ol>
            <div class="carousel-inner" role="listbox">
                <div class="item<?php echo $active === 1 ? ' active' : ''; ?>">
                    <div class="banner-col-a">
                        <img class="banner-img" src="assets/images/banner-01.jpg" width="960" height="350" alt="Soluções Inovadoras">
                    </div>
                    <div class="banner-col-b">
                        <div class="banner-content">
                            <h2 class="banner-heading">SOLUÇÕES INOVADORAS</h2>
                            <p class="banner-text">Soluções inovadoras em nutrição animal proporcionando melhor desempenho e saúde para os animais.</p>
                        </div>
                    </div>
                </div>
                <div class="item<?php echo $active === 2 ? ' active' : ''; ?>">
                    <div class="banner-col-a">
                        <img class="banner-img" src="assets/images/banner-02.jpg" width="960" height="350" alt="Agronegócio">
                    </div>
                    <div class="banner-col-b">
                        <div class="banner-content">
                            <h2 class="banner-heading">AGRONEGÓCIO</h2>
                            <p class="banner-text">Parceria com uma ampla rede de distribuidores regionais com abrangência em toda América do Sul.</p>
                        </div>
                    </div>
                </div>
                <div class="item<?php echo $active === 3 ? ' active' : ''; ?>">
                    <div class="banner-col-a">
                        <img class="banner-img" src="assets/images/banner-03.jpg" width="960" height="350" alt="Assessoria Completa">
                    </div>
                    <div class="banner-col-b">
                        <div class="banner-content">
                            <h2 class="banner-heading">ASSESSORIA COMPLETA</h2>
                            <p class="banner-text">Para empresas estrangeiras que pretendem comercializar seus produtos em toda América do Sul.</p>
                        </div>
                    </div>
                </div>
            </div>
            <a class="left carousel-control" href="#banner-carousel" role="button" data-slide="prev" title="Anterior">
                <span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span>
                <span class="sr-only">Anterior</span>
            </a>
            <a class="right carousel-control" href="#banner-carousel" role="button" data-slide="next" title="Próximo">
                <span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span>
                <span class="sr-only">Próximo</span>
            </a>
        </div>
    </div>
</div>
