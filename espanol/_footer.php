<footer>
    <div class="top">
        <div class="container">
            <div class="row">
                <div class="col-sm-8">
                    <img class="logo hidden-sm hidden-xs" src="../assets/images/footer-logo.png" width="132" height="32" alt="Biosen">
                    <nav class="hidden-sm hidden-xs">
                        <a href="">HOME</a>
                        <a href="quienes-somos">QUIÉNES SOMOS</a>
                        <a href="como-trabajamos">CÓMO TRABAJAMOS</a>
                        <a href="productos">PRODUCTOS</a>
                        <a href="socios">SOCIOS / ALIADOS</a>
                        <a href="contacto">CONTACTO</a>
                    </nav>
                </div>
                <div class="col-sm-4 text-right">
                    <span class="phone">Contáctenos +55 11 4456 4352</span>
                    <span class="phone">+55 11 4456 4358</span>
                    <a class="email hidden-xs" href="mailto:contato@biosen.com.br">contato@biosen.com.br</a>
                </div>
            </div>
        </div>
    </div>
    <p class="bottom">Biosen® Comercio de Productos Agroindustriales en América del Sur. Todos los derechos reservados.</p>
</footer>