<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <h3>Biosen tiene una localización privilegiada, cercana al principal aeropuerto de cargas y al puerto marítimo más grande en el movimiento de cargas del país.</h3>
            </div>
            <div class="col-md-7">
                <p>Con sede en la ciudad de Salto, Estado de São Paulo, la localización de Biosen tiene el privilegio de estar a 30 km del principal aeropuerto brasileño de cargas (Aeropuerto de Viracopos - Campinas - SP), a 100 km de la capital del Estado de São Paulo y a 150 km del puerto marítimo de Santos - SP, uno de los principales puertos de cargas de nuestro país.</p>
                <p class="address">
                    <span>Rua Santa Rosália, 1.195</span>
                    <span>Salto/SP - Brasil - CEP: 13.327-370</span>
                    <span class="phone">Fone: +55 11 4456 4352 / +55 11 4456 4358</span>
                    <a href="mailto:contato@biosen.com.br">contato@biosen.com.br</span>
                </p>
            </div>
            <div class="col-md-12">
                <img class="img-responsive" src="../assets/images/map.jpg" width="948" height="350" alt="">
            </div>
        </div>
    </div>
</div>