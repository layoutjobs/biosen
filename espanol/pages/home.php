<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>ASESORÍA COMPLETA A LAS EMPRESAS EXTRANJERAS EN LA COMERCIALIZACIÓN DE SUS PRODUCTOS EN AMERICA DEL SUR.</h2>
            </div>
            <div class="col-md-5">
                <h3>Tenemos una sólida red de negocios en toda América del Sur.</h3>
                <img class="img-responsive" src="../assets/images/side-home.jpg" width="379" height="323" alt="">
            </div>
            <div class="col-md-7">
                <p>Created from an innovative proposal of experience in the agro-industrial sector in South America, BIOSEN is a trading company specialized in introduce new products and technical concepts in the market. Furthermore BIOSEN has a wide network partnership with regional distributors covering throughout South America and a qualified commercial team. Our company is able to introduce quickly and consistently innovative products in the countries which have commercial activities.</p>
                <p>With a team of professionals with extensive experience in the market and a wide network of relationships with consultants in several areas of knowledge, BIOSEN is prepared to assist its PARTNERS in many steps of marketing your products, such as: feasibility analysis of products, support the product registration, importing and marketing of products itself.</p>
                <p class="slogan">BIOSEN: Breaking down cultural, linguistic and market barriers.</p>
                <ul>
                    <li class="title">BUSINESS ACTIVITIES:</li>
                    <li>Animal Nutrition</li>
                    <li>Animal Health</li>
                    <li>Household Cleaning</li>
                    <li>Specialities</li>
                </ul>
            </div>
        </div>
    </div>
</div>


    


















