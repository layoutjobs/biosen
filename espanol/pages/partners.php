<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <h3>Através de una asociación, Biosen crea una confianza comercial sólida y segura.</h3>
            </div>
            <div class="col-md-7">
                <p>Conozca a nuestros socios/aliados a continuación.<br>Haga clic sobre la imagen para visitar el sitio.</p>
                <div class="row">
                    <a class="col-md-4" href="http://www.agromed.at" class="col-md-6">
                        <img class="img-responsive" width="225" height="120" src="../assets/images/partner-logo-agromed.png" alt="AGROMED">
                        <span>Aditivos y suplementos para la alimentación animal</span>
                    </a>
                    <a class="col-md-4" href="http://www.framelco.com" class="col-md-6">
                        <img class="img-responsive" width="225" height="120" src="../assets/images/partner-logo-framelco.png" alt="FRAMELCO">
                        <span>FRAMELCO</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>





