<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <h3>Un equipo de personas cualificadas unidas por un objetivo común.</h3>
                <img class="img-responsive" src="../assets/images/side-about.jpg" width="379" height="346" alt="">
            </div>
            <div class="col-md-7">
                <p>Somos una empresa de distribución y representación comercial en toda América del Sur integrada por profesionales con un historial de éxito en la gestión comercial y estratégica en las empresas en las que trabajaban anteriormente. Unidos por un objetivo común, BIOSEN puede ser considerada como una "cooperativa de talentos" con habilidades complementarias para el éxito de los proyectos comerciales de empresas extranjeras en el sector agroindustrial.</p>
                <p>Nuestra diferencia radica en poner a disposición de nuestros socios PROVEEDORES un amplio panorama de los distintos factores relacionados con el proceso de venta de sus productos en el mercado de América del Sur. Estableciendo una relación de confianza y transparencia para la comercialización de productos de una forma sólida y segura.</p>
                <p class="slogan">EQUIPO DE PROFESIONALES:</p>
                <div class="row">
                    <div class="col-md-3">
                        <img class="img-responsive" src="../assets/images/foto-fernando-toledano.png" alt="">
                    </div>

                    <div class="col-md-9">
                        <p><b>Fernando Toledano - Director General.</b> <br>  Veterinario con 15 años de experiencia en los mercados de salud y nutrición animal. Posee MBA en Gestión de Negocios y Posgrado en Farmacología. Toledano trabajó en compañías importantes del sector de del agronegocio, tales como Schering -Plough (MSD) y Ourofino, acumulando funciones de dirección técnica, comercial y marketing en todo el territorio brasileño. Como experiencia internacional, trabajó en la empresa alemana J. Rettenmaier, donde desarrolló el cargo de gerente de negocios y fue responsable directo por la aplicación del concepto de uso de fibras insolubles en la alimentación animal.</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <img class="img-responsive" src="../assets/images/foto-luiz-kasper.png" alt="">
                    </div>
                    <div class="col-md-9">
                        <p><b>Marcelo Figueiredo Carlone - Supervisor de Ventas.</b> <br> Veterinario con experiencia en los mercados de salud y nutrición de los animales. MBA en Agronegocios. Marcelo se desempeñó como Asistente Técnico Comercial en distribuidor importante de productos veterinarios en Brasil, en representación de marcas relevantes de la industria tales como MSD Salud Animal, Agroceres Multimix, DuPont, Farmabase entre otros. Actualmente es responsable de la coordinación técnica y comercial de los clientes y distribuidores presentes en su territorio de operación (SP, MG, ES, RJ, CO y NE).</p>
                    </div>
                </div>      
            </div>
        </div>
    </div>
</div>


