<div class="content">
    <div class="container">
        <div class="row"> 
            <div class="col-md-5">
                <h3>Amplia red de distribución con cobertura en toda America del Sur.</h3>
                <img class="img-responsive" src="../assets/images/side-services.jpg" width="379" height="323" alt="">
            </div>
            <div class="col-md-7">
                <p>BIOSEN tiene un modelo de negocios innovador, que ofrece a nuestros SOCIOS/ALIADOS un alto nivel de servicios de comercialización de productos en el sector agroindustrial, destacándose por la eficiencia en el logro de los objetivos establecidos y la seguridad y la transparencia de nuestras acciones.</p>
                <p>Con una amplia red de socios DISTRIBUIDORES, BIOSEN se convierte en una plataforma de acceso para las empresas extranjeras para establecer sus actividades comerciales en toda América del Sur.</p>
                <p>A los socios/aliados DISTRIBUIDORES socios, BIOSEN les ofrece, de manera constante y continua, una serie de productos innovadores y exclusivos para la distribución regional, promoviendo ingresos crecientes y sustentabilidad para los negocios.</p>
                <p class="slogan">NUESTROS PUNTOS FUERTES:</p>
                <p><b>1. VISIÓN ESTRATÉGICA:</b> BIOSEN ofrece un análisis técnico completo, comercial y de mercado de cada producto antes de que comience el proceso de registro de productos, evitando errores de inversión y creando una base para las estrategias comerciales futuras.</p>
                <p><b>2. RED DE RELACIONES:</b> una amplia red de distribuidores regionales con cobertura en toda América del Sur le permite a BIOSEN la rápida introducción de nuevos conceptos en el mercado y asegura una buena base de ventas de los productos trabajados. Además, BIOSEN tiene fácil acceso a los principales clientes del sector agroindustrial, facilitando las ventas directas en grandes volúmenes.</p>
                <p><b>3. ENFOQUE EN PRODUCTOS INNOVADORES:</b> BIOSEN cuenta con un equipo cualificado capaz de trabajar con productos que requieran un alto nivel de conocimiento y desempeño en el campo. Esta capacidad nos permite introducir nuevos conceptos de productos y tecnologías en el mercado.</p>
                <p><b>4. TRANSPARENCIA:</b> Los informes sobre los resultados de ventas y las principales actividades desarrolladas en los clientes y distribuidores regionales se envían a nuestros SOCIOS/ALIADOS cada 4 meses.</p>
                <p><b>5. SEGURIDAD:</b> El registro de los productos se hace en el nombre de BIOSEN, por exigencia de las leyes brasileñas, pero el dominio sobre la propiedad de los productos permanece en nombre de nuestros SOCIOS/ALIADOS. Esto asegura que el control del negocio permanezca en manos de nuestros SOCIOS/ALIADOS.</p>
                <p><b>6. FACILIDAD DE COMUNICACIÓN:</b> Todo el equipo de ventas y de operaciones internas es capaz de comunicarse en inglés y español.</p>
                <p class="slogan">MODELO DE NEGOCIOS:</p>  
                <p><b>Brasil:</b> Distribución exclusiva.</p>
                <ul>
                    <li>Ventas directas a grandes clientes;</li>
                    <li>Ventas indirectas a pequeños y medianos clientes a través de distribuidores regionales;</li>
                    <li>Intermediación de ventas para clientes especiales.</li>
                </ul>
                <br><br>
                <p><b>Otros países de América del Sur:</b> Representación comercial.</p>
            </div>
        </div>
    </div>
</div>