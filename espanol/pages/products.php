<div class="content">
    <div class="container">
        <div class="product row">
            <div class="title col-xs-4 col-sm-5">LECIMAX</div>
            <div class="col-xs-8 col-sm-7">
                <p>Emulsionante de alta performance a base de Lecitina Hidrolisada</p>
                <p><a href="../assets/downloads/folheto_lecimax_biosen.pdf" target="_blank"><i class="glyphicon glyphicon-file"></i> PDF</a></p>
            </div>
        </div>
        <div class="product row">
            <div class="title col-xs-4 col-sm-5">OPTICELL C5</div>
            <div class="col-xs-8 col-sm-7">
                <p>Ingrediente nutricional que consta de una combinación sinérgica de fibra fermentable e no fermentable para alimentar a las aves y los cerdos.</p>
                <p><a href="../assets/downloads/opticell-es.pdf" target="_blank"><i class="glyphicon glyphicon-file"></i> PDF</a></p>
            </div>
        </div>
        <div class="product row">
            <div class="title col-xs-4 col-sm-5">OPTICELL C2</div>
            <div class="col-xs-8 col-sm-7">Ingrediente nutricional que consta de una combinación sinérgica de fibra fermentable e no fermentable para alimentar a los pets.</div>
        </div>
        <div class="product row">
            <div class="title col-xs-4 col-sm-5">FIBRECELL</div>
            <div class="col-xs-8 col-sm-7">
                <p>Concentrado de fibra no fermentable con alta capacidad de estímulo de la saciedad para aves y cerdos.</p>
                <p><a href="../assets/downloads/fibrecell-pt.pdf" target="_blank"><i class="glyphicon glyphicon-file"></i> PDF</a></p>
            </div>
        </div>
        <div class="product row">
            <div class="title col-xs-4 col-sm-5">TIEMPO 305</div>
            <div class="col-xs-8 col-sm-7">
                <p>Aditivo nutricionale de baja inclusión para alimentación de los rumiantes. Regulador de la actividad ruminal.</p>
                <p><a href="../assets/downloads/time-305-en.pdf" target="_blank"><i class="glyphicon glyphicon-file"></i> PDF</a></p>
            </div>
        </div>
        <div class="product row">
            <div class="title col-xs-4 col-sm-5"><img src="../assets/images/logo-fra-butrin-hybrid-dry.png" alt="FRA Butyrin Hybrid Dry"></div>
            <div class="col-xs-8 col-sm-7">
                <p>Aditivo alimenticio que contiene MONOBUTIRINA indicado como estimulante de la producción y el control de enfermedades causadas por bacterias GRAM NEGATIVO.</p>
                <p><a href="../assets/downloads/fra-butyrin-hybrid-dry-pt.pdf" target="_blank"><i class="glyphicon glyphicon-file"></i> PDF</a></p>
            </div>
        </div>
        <div class="product row">
            <div class="title col-xs-4 col-sm-5"><img src="../assets/images/logo-fra-c12-dry.png" alt="FRA C12 Dry"></div>
            <div class="col-xs-8 col-sm-7">
                <p>Aditivo alimenticio que contiene MONOLAURINA indicado como estimulante de la producción y el control de enfermedades causadas por bacterias GRAM POSITIVO.</p>
                <p><a href="../assets/downloads/fra-c12-dry-pt.pdf" target="_blank"><i class="glyphicon glyphicon-file"></i> PDF</a></p>
            </div>
        </div>
        <!--<div class="product row bg-primary">
            <div class="title col-xs-4 col-sm-5">PROGRAMA OPTIZinc</div>
            <div class="col-xs-8 col-sm-7">
                <p>Concepto innovador para el control de enfermedades entéricas en aves y cerdos, favoreciendo  mejor rendimiento de los animales sin antibióticos.</p>
                <p><a href="../assets/downloads/optizinc-pt.pdf" target="_blank"><i class="glyphicon glyphicon-file"></i> PDF</a></p>
            </div>-->
        </div>
    </div>
</div>
