<header class="header">
    <div class="container">
        <div class="header-bar">
            <a class="header-bar-email" href="mailto:contato@biosen.com.br">
                <span class="glyphicon glyphicon-envelope"></span>
                contato@biosen.com.br
            </a>
            <div class="header-bar-phone-list">
                <span class="glyphicon glyphicon-earphone"></span>
                <a class="header-bar-phone-list-item" href="tel:551144564352">+55 11 4456.4352</a>
                <a class="header-bar-phone-list-item" href="tel:551144564352">+55 11 4456.4352</a>
            </div>
            <div class="header-bar-localization-nav">
                <a class="header-bar-localization-nav-item active" href="../">
                    <span class="localization-flag localization-flag-pt" title="Português"></span>
                    <span class="sr-only">Português</span>
                </a>
                <a class="header-bar-localization-nav-item" href="../english">
                    <span class="localization-flag localization-flag-en" title="English"></span>
                    <span class="sr-only">English</span>
                </a>
                <a class="header-bar-localization-nav-item" href="../espanol">
                    <span class="localization-flag localization-flag-es" title="Español"></span>
                    <span class="sr-only">Español</span>
                </a>
            </div>
        </div>
        <div class="header-brand">
            <a class="header-brand-logo" href="">
                <img src="../assets/images/logo.png" width="240" height="80" alt="Biosen - Comércio de produtos Agroindustriais na América do Sul">
            </a>
            <button class="header-nav-toggle btn" data-toggle="collapse" data-target="#header-nav" aria-controls="header-nav">
                <span class="glyphicon glyphicon-menu-hamburger"></span>
            </button>
        </div>
        <nav class="header-nav collapse" id="header-nav">
            <a class="header-nav-item<?php echo $page == 'home' ? ' active' : ''; ?>" href="">HOME</a>
            <a class="header-nav-item<?php echo $page == 'about' ? ' active' : ''; ?>" href="about-us">ABOUT US</a>
            <a class="header-nav-item<?php echo $page == 'services' ? ' active' : ''; ?>" href="how-we-work">HOW WE WORK</a>
            <a class="header-nav-item<?php echo $page == 'products' ? ' active' : ''; ?>" href="products">PRODUCTS</a>
            <a class="header-nav-item<?php echo $page == 'partners' ? ' active' : ''; ?>" href="partners">PARTNERS</a>
            <a class="header-nav-item" href="http://blog.biosen.com.br">NEWS</a>
            <a class="header-nav-item<?php echo $page == 'contact' ? ' active' : ''; ?>" href="contact">CONTACT</a>
        </nav>
    </div>
</header>
