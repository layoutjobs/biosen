<div class="content">
    <div class="container">
        <div class="product row">
            <div class="title col-xs-4 col-sm-5">LECIMAX</div>
            <div class="col-xs-8 col-sm-7">
                <p>High performance emulsifier based on Hydrolyzed Lecithin.</p>
                <p><a href="../assets/downloads/folheto_lecimax_biosen.pdf" target="_blank"><i class="glyphicon glyphicon-file"></i> PDF</a></p>
            </div>
        </div>
        <div class="product row">
            <div class="title col-xs-4 col-sm-5">OPTICELL C5</div>
            <div class="col-xs-8 col-sm-7">
                <p>Nutritional ingredient consisting of a synergistic combination of fermentable and non-fermentable fiber to feed birds and swines.</p>
                <p><a href="../assets/downloads/opticell-pt.pdf" target="_blank"><i class="glyphicon glyphicon-file"></i> PDF</a></p>
            </div>
        </div>
        <div class="product row">
            <div class="title col-xs-4 col-sm-5">OPTICELL C2</div>
            <div class="col-xs-8 col-sm-7">Nutritional ingredient consisting of a synergistic combination of fermentable and non-fermentable fiber to feed pets.</div>
        </div>
        <div class="product row">
            <div class="title col-xs-4 col-sm-5">FIBRECELL</div>
            <div class="col-xs-8 col-sm-7">
                <p>Concentrate of non-fermentable fiber with high capacity of satiety stimulus for birds and swines.</p>
                <p><a href="../assets/downloads/fibrecell-pt.pdf" target="_blank"><i class="glyphicon glyphicon-file"></i> PDF</a></p>
            </div>
        </div>
        <div class="product row">
            <div class="title col-xs-4 col-sm-5">TIME 305</div>
            <div class="col-xs-8 col-sm-7">
                <p>Low inclusion nutritional additive for ruminant feed. Regulator of ruminal activity.</p>
                <p><a href="../assets/downloads/time-305-en.pdf" target="_blank"><i class="glyphicon glyphicon-file"></i> PDF</a></p>
            </div>
        </div>
        <div class="product row">
            <div class="title col-xs-4 col-sm-5"><img src="../assets/images/logo-fra-butrin-hybrid-dry.png" alt="FRA Butyrin Hybrid Dry"></div>
            <div class="col-xs-8 col-sm-7">
                <p>Nutritional additive containing MONOBUTYRIN indicated as performance improver and control of diseases caused by bacteria GRAM NEGATIVE.</p>
                <p><a href="../assets/downloads/fra-butyrin-hybrid-dry-pt.pdf" target="_blank"><i class="glyphicon glyphicon-file"></i> PDF</a></p>
            </div>
        </div>
        <div class="product row">
            <div class="title col-xs-4 col-sm-5"><img src="../assets/images/logo-fra-c12-dry.png" alt="FRA C12 Dry"></div>
            <div class="col-xs-8 col-sm-7">
                <p>Nutritional additive containing MONOLAURIN indicated as performance improver and control of diseases caused by bacteria GRAM POSITIVE.</p>
                <p><a href="../assets/downloads/fra-c12-dry-pt.pdf" target="_blank"><i class="glyphicon glyphicon-file"></i> PDF</a></p>
            </div>
        </div>
        <!--<div class="product row bg-primary">
            <div class="title col-xs-4 col-sm-5">PROGRAMA OPTIZinc</div>
            <div class="col-xs-8 col-sm-7">
                <p>Innovative concept for enteric diseases control in birds and swines, promoting better animal performance without antibiotics.</p>
                <p><a href="../assets/downloads/optizinc-pt.pdf" target="_blank"><i class="glyphicon glyphicon-file"></i> PDF</a></p>
            </div>-->
        </div>
    </div>
</div>
