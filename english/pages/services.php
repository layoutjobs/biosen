<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <h3>Wide distribution network covering troughout South America</h3>
                <img class="img-responsive" src="../assets/images/side-services.jpg" width="379" height="323" alt="">
            </div>
            <div class="col-md-7">
                <p>BIOSEN has an innovative business model, offering to our PARTNERS a high level of services to commercialize products in the agro-industrial sector, highlighting the efficient achievement of goals and the security and transparency in our actions.</p>
                <p>We also have a wide network partnership with regional DISTRIBUTORS, BIOSEN becomes an access platform for foreign companies to establish their business activities throughout South America.</p>
                <p>For DISTRIBUTORS, BIOSEN offers, steadily and continuously, a several innovative and unique products to regional distribution, promoting an additional income and increasing sustainability for business.</p>
                <p class="slogan">OUR ADVANTAGES:</p>
                <p><b>1. STRATEGIC VISION:</b> BIOSEN offers a complete technical, trading and market analysis for each product before the beginning of the process of product registration, avoiding mistakes in investments and creating a foundation for future business strategies.</p>
                <p><b>2. NETWORK OF RELATIONSHIP:</b> A wide network relationship with regional DISTRIBUTORS covering throughout South America allows BIOSEN fast introduction of new concepts in the market and ensures a good sales foundations for the products that we are working with. In addition, BIOSEN has easy access to major key customers in the agro-industrial sector, facilitating direct sales in large volumes.</p>
                <p><b>3. FOCUS ON INNOVATIVE PRODUCTS:</b> BIOSEN has a qualified staff able to work with products that require high level of knowledge and performance in the field. This ability allows us to introduce new products concepts and technologies in the market.</p>
                <p><b>4. TRANSPARENCY:</b> Reports on sales results and main commercial activities developed with customers and regional distributors are sent to our PARTNERS every 4 months.</p>
                <p><b>5. SAFETY:</b> The product registration is done in BIOSEN’s name, as required by Brazilian law, but the domain over the product property remains on behalf of our PARTNERS. This ensures that the control of the business remains in the hands of our PARTNERS.</p>
                <p><b>6. EASY OF COMMUNICATION:</b> The whole sales team and internal operations are able to communicate in English and Spanish.</p>
                <p class="slogan">BUSINESS MODEL:</p>
                <p><b>Brazil:</b> Exclusive distribution.</p>
                <ul>
                    <li>Direct sales to large customers;</li>
                    <li>Indirect sales to small and middle customers throughout regional distributor;</li>
                    <li>Sales intermediation to special customers.</li>
                </ul>
                <br><br>
                <p><b>Other countries in South America:</b> Trade agent.</p>
            </div>
        </div>
    </div>
</div>