<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <h3>A team of skilled people united by a common goal</h3>
                <img class="img-responsive" src="../assets/images/side-about.jpg" width="379" height="346" alt="">
            </div>
            <div class="col-md-7">
                <p>We are a company of distribution and commercial representation throughout South America composed by professionals with success history in business and strategic management in companies that we previously had worked. United by a common goal, BIOSEN can be considered a "talent cooperative" with complementary skills working to achieve the success of commercial projects of foreign companies in the agro-industrial sector.</p>
                <p>Our difference is provide to our PARTNERS a wide overview in several factors related to the process of selling their products in the market in South America establishing a relationship with trust and transparency to commercialize products in a solid and safe way.</p>
                <p class="slogan">STAFF:</p>
                <div class="row">
                    <div class="col-md-3">
                        <img class="img-responsive" src="../assets/images/foto-fernando-toledano.png" alt="">
                    </div>
                    <div class="col-md-9">
                        <p><b>Fernando Toledano – Director.</b> <br> Veterinarian with over 15 years of experience in the veterinary and animal nutrition markets. MBA in Business Management and Graduate studies in Pharmacology. Toledano has worked in relevant companies of Brazilian agribusiness, such as Schering-Plough and Ourofino accumulating functions of commercial, technical and marketing management. As international experience he had worked in a German company - J.Rettenmaier, where he developed the role of business manager, being directly responsible for the implementation of the concept of insoluble fiber in the nutrition of animals in Brazil.</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <img class="img-responsive" src="../assets/images/foto-luiz-kasper.png" alt="">
                    </div>
                    <div class="col-md-9">
                        <p><b>Luiz Kasper – Commercial Manager.</b> <br> Veterinarian with over 26 years of experience in the veterinary and animal nutrition markets. Graduate studies in Commercial Management and Nutrition of Monogastric Animals. Kasper has extensive experience attending key accounts customers in Brazil, Paraguay and Uruguay by companies like Trouw Nutrition, Novus and Vaccinar. Kasper has great access to the main key accounts customers in feed segment and skill in developing and management of the sales team and distributor.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


