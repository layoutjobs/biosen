<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <h3>Through partnership, Biosen creates a solid and secure commercial relationship of trust.</h3>
            </div>
            <div class="col-md-7">
                <p>Meet our partners below.<br>Click on the image to visit the site.</p>
                <div class="row">
                    <a class="col-md-4" href="http://www.agromed.at" class="col-md-6">
                        <img class="img-responsive" width="225" height="120" src="../assets/images/partner-logo-agromed.png" alt="AGROMED">
                        <span>Solution in dietary fibers</span>
                    </a>
                    <a class="col-md-4" href="http://www.framelco.com" class="col-md-6">
                        <img class="img-responsive" width="225" height="120" src="../assets/images/partner-logo-framelco.png" alt="FRAMELCO">
                        <span>FRAMELCO</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>








