<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <h3>Biosen has a privileged location, close to the main airport and the biggest load sea port cargo handling in the caountry.</h3>
            </div>
            <div class="col-md-7">
                <p>Headquartered in Salto City in Sao Paulo State, Biosen has a good location because it is 30km from the main cargo airport in Brazil (Viracopos Airport - Campinas - SP), 100km from the capital of São Paulo and 150km from seaport of Santos - SP, one of the major shipping ports of our country.</p>
                <p class="address">
                    <span>1.195, Santa Rosália Str </span>
                    <span>Salto/SP - Brazil - ZIP CODE: 13.327-370</span>
                    <span class="phone">Phone: +55 11 4456 4352 / +55 11 4456 4358</span>
                    <a href="mailto:contato@biosen.com.br">contato@biosen.com.br</span>
                </p>
            </div>
            <div class="col-md-12">
                <img class="img-responsive" src="../assets/images/map.jpg" width="948" height="350" alt="">
            </div>
        </div>
    </div>
</div>