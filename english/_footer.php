<footer>
    <div class="top">
        <div class="container">
            <div class="row">
                <div class="col-sm-8">
                    <img class="logo hidden-sm hidden-xs" src="../assets/images/footer-logo.png" width="132" height="32" alt="Biosen">
                    <nav class="hidden-sm hidden-xs">
                        <a href="">HOME</a>
                        <a href="about-us">ABOUT US</a>
                        <a href="how-we-work">HOW WE WORK</a>
                        <a href="products">PRODUCTS</a>
                        <a href="partners">PARTNERS</a>
                        <a href="contact">CONTACT</a>
                    </nav>
                </div>
                <div class="col-sm-4 text-right">
                    <span class="phone">Contact us +55 11 4456 4352</span>
                    <span class="phone">+55 11 4456 4358</span>
                    <a class="email" href="mailto:contato@biosen.com.br">contato@biosen.com.br</a>
                </div>
            </div>
        </div>
    </div>
    <p class="bottom">Biosen® Trade of Agro-Industrial Products in South America. All rights reserved.</p>
</footer>