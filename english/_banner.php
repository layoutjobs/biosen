<?php $active = rand(1, 3); ?>

<div class="banner">
    <div class="container">
        <div id="banner-carousel" class="carousel" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#banner-carousel" data-slide-to="0"<?php echo $active === 1 ? ' class="active"' : ''; ?>></li>
                <li data-target="#banner-carousel" data-slide-to="1"></li>
                <li data-target="#banner-carousel" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner" role="listbox">
                <div class="item<?php echo $active === 1 ? ' active' : ''; ?>">
                    <div class="banner-col-a">
                        <img class="banner-img" src="../assets/images/banner-01.jpg" width="960" height="350" alt="Soluções Inovadoras">
                    </div>
                    <div class="banner-col-b">
                        <div class="banner-content">
                            <h2 class="banner-heading">INNOVATIVE SOLUTIONS</h2>
                            <p class="banner-text">Innovative solutions in animal feed providing better performance and health for animals.</p>
                        </div>
                    </div>
                </div>
                <div class="item<?php echo $active === 2 ? ' active' : ''; ?>">
                    <div class="banner-col-a">
                        <img class="banner-img" src="../assets/images/banner-02.jpg" width="960" height="350" alt="Agronegócio">
                    </div>
                    <div class="banner-col-b">
                        <div class="banner-content">
                            <h2 class="banner-heading">AGRIBUSINESS</h2>
                            <p class="banner-text">Partnership with an extensive network of regional distributors with coverage throughout South America.</p>
                        </div>
                    </div>
                </div>
                <div class="item<?php echo $active === 3 ? ' active' : ''; ?>">
                    <div class="banner-col-a">
                        <img class="banner-img" src="../assets/images/banner-03.jpg" width="960" height="350" alt="Assessoria Completa">
                    </div>
                    <div class="banner-col-b">
                        <div class="banner-content">
                            <h2 class="banner-heading">COMPLETE ADVISORY</h2>
                            <p class="banner-text">For foreign companies seeking to commercialize their products throughout South America.</p>
                        </div>
                    </div>
                </div>
            </div>
            <a class="left carousel-control" href="#banner-carousel" role="button" data-slide="prev" title="Anterior">
                <span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span>
                <span class="sr-only">Anterior</span>
            </a>
            <a class="right carousel-control" href="#banner-carousel" role="button" data-slide="next" title="Próximo">
                <span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span>
                <span class="sr-only">Próximo</span>
            </a>
        </div>
    </div>
</div>
