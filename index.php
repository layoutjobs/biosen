<?php require 'api/core.php'; ?>
<!doctype html>
<html lang="pt-br">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta name="author" content="Layout - http://layoutnet.com.br">
    <title>Biosen</title>
    <base href="<?php echo Request::getBaseUrl(); ?>">
    <link rel="stylesheet" href="assets/styles/main-dist.css">
	<link rel="stylesheet" href="//fonts.googleapis.com/css?family=Open+Sans:300,400,700">
    <!--[if lt IE 9]><script src="assets/js/html5.js"></script><![endif]-->
</head>
<body>
    <?php
    extract($_GET);
    require '_header.php';
    require '_banner.php';
    require 'pages/' . $page . '.php';
    require '_footer.php';
    ?>
    <script src="assets/scripts/main-dist.js"></script>
</body>
</html>
