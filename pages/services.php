<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <h3>Ampla rede de distribuição com abrangência em toda América do Sul.</h3>
                <img class="img-responsive" src="assets/images/side-services.jpg" width="379" height="323" alt="">
            </div>
            <div class="col-md-7">
                <p>A BIOSEN possui um modelo de negócios inovador, oferecendo aos nossos PARCEIROS um alto nível de serviços para comercialização de produtos do setor agroindustrial, destacando-se pela eficiência no atingimento de metas estabelecidas e a segurança e transparência de nossas ações.</p>
                <p>Com uma ampla rede de parceiros DISTRIBUIDORES, a BIOSEN torna-se uma plataforma de acesso para as empresas estrangeiras para estabelecer suas atividades comerciais em toda a América do Sul.</p>
                <p>Aos parceiros DISTRIBUIDORES, a BIOSEN oferece, de forma constante e contínua, uma série de produtos inovadores e exclusivos para distribuição regional, promovendo um adicional crescente de receita e sustentabilidade para o negócio.</p>
                <p class="slogan">NOSSOS DIFERENCIAIS:</p>
                <p><b>1. VISÃO ESTRATÉGICA:</b> A BIOSEN oferece uma completa análise técnica, comercial e de mercado para cada produto antes do início do processo de registro de produtos, evitando erros de investimento e criando uma base para futuras estratégias comerciais.</p>
                <p><b>2. REDE DE RELACIONAMENTO:</b> Uma ampla rede de distribuidores regionais com abrangência em toda a América do Sul permite à BIOSEN uma rápida introdução de novos conceitos no mercado e assegura uma boa base de vendas para os produtos trabalhados. Além disto, a BIOSEN possui fácil acesso aos principais clientes chave do setor agroindustrial, facilitando vendas diretas em grandes volumes.</p>
                <p><b>3. FOCO EM PRODUTOS INOVADORES:</b> A BIOSEN possui uma equipe qualificada capaz de trabalhar com produtos que demandem alto nível de conhecimento e desempenho a campo. Esta habilidade nos permite introduzir novos conceitos de produtos e tecnologias no mercado.</p>
                <p><b>4. TRANSPARÊNCIA:</b> Relatórios sobre resultados de vendas e principais atividades comerciais desenvolvidas em clientes e distribuidores regionais são enviadas aos nossos PARCEIROS a cada 4 meses.</p>
                <p><b>5. SEGURANÇA:</b> O registro dos produtos é feito em nome da BIOSEN, por exigência das leis brasileiras, mas o domínio sobre a propriedade dos produtos permanece em nome de nossos PARCEIROS. Isto assegura que o controle do negócio permanece nas mãos de nossos PARCERIOS.</p>
                <p><b>6. FACILIDADE DE COMUNICAÇÃO:</b> Toda a equipe de vendas e de operações internas é capaz de se comunicar em inglês e espanhol.</p>
                <p class="slogan">MODELO DE NEGÓCIOS:</p>
                <p><b>Brasil:</b> Distribuição exclusiva.</p>
                <ul>
                    <li>Vendas diretas a grandes clientes;</li>
                    <li>Vendas indiretas a pequenos e médios clientes através de distribuidores regionais;</li>
                    <li>Intermediação de vendas para clientes especiais.</li>
                </ul>
                <br><br>
                <p><b>Outros países da América do Sul:</b> Representação comercial.</p>
            </div>
        </div>
    </div>
</div>