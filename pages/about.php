<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-md-5">   
                <h3>Um time de pessoas capacitadas unidas por um objetivo em comum.</h3>
                <img class="img-responsive" src="assets/images/side-about.jpg" width="379" height="346" alt="">
            </div>
            <div class="col-md-7">
                <p>Somos uma empresa de distribuição e representação comercial para toda a América do Sul composta por profissionais com história de sucesso na gestão comercial e estratégica nas empresas nas quais trabalharam anteriormente. Unidos por um objetivo comum, a BIOSEN pode ser considerada como uma “cooperativa de talentos” com habilidades complementares para o sucesso de projetos comerciais de empresas estrangeiras do setor agroindustrial.</p>
                <p>Nosso diferencial está em disponibilizar aos nossos parceiros FORNECEDORES uma visão ampla dos vários fatores relacionados ao processo de vendas de seus produtos no mercado da América do Sul. Estabelecendo uma relação de confiança e transparência para comercialização de produtos de forma sólida e segura.</p>
                <p class="slogan">EQUIPE DE PROFISSIONAIS:</p>
                <div class="row">
                    <div class="col-md-3">
                        <img class="img-responsive" src="assets/images/foto-fernando-toledano.png" alt="">
                    </div>

                    <div class="col-md-9">
                        <p><b>Fernando Toledano – Diretor Geral.</b> <br> Veterinário com 15 anos de experiência nos mercados de saúde e nutrição animal. MBA em Gestão de Negócios e Pós-Graduação em Farmacologia. Toledano trabalhou em empresas relevantes do setor agronegócio, tais como Schering-Plough (MSD) e Ourofino, acumulando funções de gerência técnica, comercial e marketing em todo território brasileiro. Como experiência internacional, trabalhou na empresa alemã - J.Rettenmaier, onde desenvolveu função de gerente de negócios e sendo o responsável direto pela implantação do conceito de uso de fibras insolúveis na nutrição animal.</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <img class="img-responsive" src="assets/images/foto-luiz-kasper.png" alt="">
                    </div>
                    <div class="col-md-9">
                        <p><b>Luiz Kasper – Gerente Comercial.</b> <br> Veterinário com mais de 25 anos de experiência nos mercados de saúde e nutrição animal. Pós-Graduação em Gerenciamento de Negócios e Nutrição de Animais Monogástricos. Kasper tem uma larga experiência no atendimento de grandes clientes (Key accounts) e rede de distribuidores no Brasil, Paraguai e Uruguai. Tendo trabalhado em empresas como Trouw Nutrition, Novus e Vaccinar. Kasper tem um bom acesso a clientes chaves em nutrição animal e habilidade no desenvolvimento e gerenciamento de equipe de vendas.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>