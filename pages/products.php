<div class="content">
    <div class="container">
	    <div class="product row">
            <div class="title col-xs-4 col-sm-5">LECIMAX</div>
            <div class="col-xs-8 col-sm-7">
                <p>Emulsificante de alta eficiência a base de Lecitina Hidrolisada</p>
                <p><a href="assets/downloads/folheto_lecimax_biosen.pdf" target="_blank"><i class="glyphicon glyphicon-file"></i> PDF</a></p>
            </div>
        </div>
        <div class="product row">
            <div class="title col-xs-4 col-sm-5">OPTICELL C5</div>
            <div class="col-xs-8 col-sm-7">
                <p>Ingrediente nutricional composto por uma combinação sinérgica de fibras fermentáveis e não fermentáveis para alimentação de aves e suínos.</p>
                <p><a href="assets/downloads/opticell-pt.pdf" target="_blank"><i class="glyphicon glyphicon-file"></i> PDF</a></p>
            </div>
        </div>
        <div class="product row">
            <div class="title col-xs-4 col-sm-5">OPTICELL C2</div>
            <div class="col-xs-8 col-sm-7">Ingrediente nutricional composto por uma combinação sinérgica de fibras fermentáveis e não fermentáveis para alimentação de animais de estimação.</div>
        </div>
        <div class="product row">
            <div class="title col-xs-4 col-sm-5">FIBRECELL</div>
            <div class="col-xs-8 col-sm-7">
                <p>Concentrado de fibra não fermentável com alta capacidade de estímulo de saciedade para aves e suínos.</p>
                <p><a href="assets/downloads/fibrecell-pt.pdf" target="_blank"><i class="glyphicon glyphicon-file"></i> PDF</a></p>
            </div>
        </div>
        <div class="product row">
            <div class="title col-xs-4 col-sm-5">TIME 305</div>
            <div class="col-xs-8 col-sm-7">
                <p>Aditivo nutricional de baixa inclusão para alimentação de ruminantes. Regulador da atividade ruminal.</p>
                <p><a href="assets/downloads/time-305-en.pdf" target="_blank"><i class="glyphicon glyphicon-file"></i> PDF</a></p>
            </div>
        </div>
        <div class="product row">
            <div class="title col-xs-4 col-sm-5"><img src="assets/images/logo-fra-butrin-hybrid-dry.png" alt="FRA Butyrin Hybrid Dry"></div>
            <div class="col-xs-8 col-sm-7">
                <p>Aditivo nutricional contendo MONOBUTIRINA indicado como melhorador de desempenho e o controle de doenças causadas por bactérias GRAM NEGATIVAS.</p>
                <p><a href="assets/downloads/fra-butyrin-hybrid-dry-pt.pdf" target="_blank"><i class="glyphicon glyphicon-file"></i> PDF</a></p>
            </div>
        </div>
        <div class="product row">
            <div class="title col-xs-4 col-sm-5"><img src="assets/images/logo-fra-c12-dry.png" alt="FRA C12 Dry"></div>
            <div class="col-xs-8 col-sm-7">
                <p>Aditivo nutricional contendo MONOLAURINA indicado como melhorador de desempenho e controle de doenças causadas por bactérias GRAM POSITIVAS.</p>
                <p><a href="assets/downloads/fra-c12-dry-pt.pdf" target="_blank"><i class="glyphicon glyphicon-file"></i> PDF</a></p>
            </div>
        </div>
       <!-- <div class="product row bg-primary">
            <div class="title col-xs-4 col-sm-5">PROGRAMA OPTIZinc</div>
            <div class="col-xs-8 col-sm-7">
                <p>Conceito inovador para controle de doenças entéricas em aves e suínos, promovendo melhor desempenho dos animais sem necessidade de uso de antibióticos.</p>
                <p><a href="assets/downloads/optizinc-pt.pdf" target="_blank"><i class="glyphicon glyphicon-file"></i> PDF</a></p>
            </div>
        </div>-->
    </div>
</div>
