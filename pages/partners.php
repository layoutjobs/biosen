<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <h3>Através de parcerias, a Biosen cria uma relação de confiança comercial sólida e segura.</h3>
            </div>
            <div class="col-md-7">
                <p>Conheça abaixo os nossos parceiros.<br>Clique sobre a imagem para visitar o site.</p>
                <div class="row">
                    <a class="col-md-4" href="http://www.agromed.at" class="col-md-6">
                        <img class="img-responsive" width="225" height="120" src="assets/images/partner-logo-agromed.png" alt="AGROMED">
                        <span>Soluções em fibras alimentares</span>
                    </a>
                    <a class="col-md-4" href="http://www.framelco.com" class="col-md-6">
                        <img class="img-responsive" width="225" height="120" src="../assets/images/partner-logo-framelco.png" alt="FRAMELCO">
                        <span>FRAMELCO</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>







