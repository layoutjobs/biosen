<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <h3>A Biosen possui localização privilegiada, próximo ao principal aeroporto de carga e ao maior porto marítimo em movimentação de cargas do país.</h3>
            </div>
            <div class="col-md-7">
                <p>Sediada no município de Salto no estado de São Paulo, a Biosen possui localização privilegiada por estar a 30km do principal aeroporto de carga brasileiro (Aeroporto de Viracopos - Campinas - SP), 100km da capital do Estado de São Paulo e 150km do porto marítimo de Santos - SP, um dos principais portos de carga de nosso país.</p>
                <p class="address">
                    <span>Rua Santa Rosália, 1.195</span>
                    <span>Salto/SP - Brasil - CEP: 13.327-370</span>
                    <span class="phone">Fone: +55 11 4456 4352 / +55 11 4456 4358</span>
                    <a href="mailto:contato@biosen.com.br">contato@biosen.com.br</span>
                </p>
            </div>
            <div class="col-md-12">
                <img class="img-responsive" src="assets/images/map.jpg" width="948" height="350" alt="">
            </div>
        </div>
    </div>
</div>