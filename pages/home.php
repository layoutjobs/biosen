<div class="content">
    <div class="container">
        <div class="row"> 
            <div class="col-md-12">
                <h2>ASSESSORIA COMPLETA ÀS EMPRESAS ESTRANGEIRAS NA COMERCIALIZAÇÃO DE SEUS PRODUTOS NA AMÉRICA DO SUL.</h2>
            </div>
            <div class="col-md-5">
                <h3>Possuímos uma sólida rede de negócios em toda América do Sul.</h3>
                <img class="img-responsive" src="assets/images/side-home.jpg" width="379" height="323" alt="">
            </div>
            <div class="col-md-7">
                <p>Criada a partir de uma proposta inovadora de atuação no setor agroindustrial na América do Sul, a BIOSEN é uma empresa comercial especializada na introdução de novos produtos e conceitos técnicos no mercado. Com uma ampla rede de parceiros comerciais com abrangência em toda América do Sul e uma equipe técnica-comercial capacitada, a BIOSEN é capaz de introduzir de forma rápida e consistente, produtos inovadores nos países os quais possui atividades comerciais.</p>
                <p>Com uma equipe composta por profissionais de grande experiência de mercado e uma ampla rede de relacionamento com consultores em diversas áreas de conhecimento, a BIOSEN está preparada a assessorar seus PARCEIROS nas várias etapas de comercialização de seus produtos: análise de viabilidade de produtos, suporte no registro de produtos, importação de produtos e comercialização de produtos propriamente dita.</p>
                <p class="slogan">BIOSEN: Quebrando barreiras culturais, 
                linguísticas e mercadológicas.</p>
                <ul>
                    <li class="title">SEGMENTOS DE ATUAÇÃO:</li>
                    <li>Nutrição Animal</li>
                    <li>Saúde Animal</li>
                    <li>Produtos Domissanitários</li>
                    <li>Especialidades</li>
                </ul>
            </div>
        </div>
    </div>
</div>